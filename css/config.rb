require 'sass-globbing'
preferred_syntax = :sass
http_path = '/'
css_dir = 'css'
sass_dir = 'css/sass'
images_dir = 'css/images'
javascripts_dir = 'css/js'
relative_assets = true
line_comments = true