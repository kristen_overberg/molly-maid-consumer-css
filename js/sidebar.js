$(document).ready(function(){

  // Add spacing for when Premier Block is at the top of the page
  var mainContent = $('.main-content');
  var theBlock = $('.sidebar-premier-block');

  $(window).resize(function() {
   isPremierBlockTop(Math.max(document.documentElement.clientWidth, window.innerWidth || 0), theBlock[0].scrollHeight);
  });

  isPremierBlockTop(Math.max(document.documentElement.clientWidth, window.innerWidth || 0), theBlock[0].scrollHeight);

  function isPremierBlockTop(windowWidth, theBlockHeight){    
   if (windowWidth <= 960){
     mainContent.css('padding-top', (theBlockHeight));
   } else {
     mainContent.css('padding-top', 0);
   }
  }

});