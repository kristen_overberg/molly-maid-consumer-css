$(document).ready(function(){

  // Add spacing for when PPC Premier Block is at the top of the page
  var mainContentPPC = $('.main-content-ppc');
  var theBlockPPC = $('.sidebar-premier-block-ppc');

  $(window).resize(function() {
   isPremierBlockTopPPC(Math.max(document.documentElement.clientWidth, window.innerWidth || 0), theBlockPPC[0].scrollHeight);
  });

  isPremierBlockTopPPC(Math.max(document.documentElement.clientWidth, window.innerWidth || 0), theBlockPPC[0].scrollHeight);

  function isPremierBlockTopPPC(windowWidth, theBlockHeight){    
   if (windowWidth <= 720){
     mainContentPPC.css('padding-top', (theBlockHeight));
   } else {
     mainContentPPC.css('padding-top', 0);
   }
  }

});