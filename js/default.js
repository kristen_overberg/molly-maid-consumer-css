
$(window).load(function () {

    // search toggle
    $(".search > .search-icon").off().on("touchstart click", function (event) {
        event.preventDefault();
        if ($(".search").hasClass('open')) {
            $(".search").removeClass('open');
        } else {
            $(".search").addClass('open');
        }
    });

    // close search if clicked elsewhere
    $(document).on('touchstart click', function (event) {
        if ($(".search").hasClass('open') && $(event.target).closest('.search').length < 1){
            $('.search').removeClass('open');
            event.preventDefault();
        }
    });

});