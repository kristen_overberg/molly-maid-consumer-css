var gulp = require('gulp');

var compass    = require('gulp-compass'); 
var plumber    = require('gulp-plumber');
var watch      = require('gulp-watch');
var webserver  = require('gulp-webserver');
var connect    = require('gulp-connect'); 
var pixrem     = require('gulp-pixrem'); 
var prefix     = require('gulp-autoprefixer');
var svgSprite  = require('gulp-svg-sprites');
var svg2png    = require('gulp-svg2png');
var minifyCSS  = require('gulp-minify-css');

// Error fix
var watching = false;
 
function onError(err) {
  console.log(err.toString());
  if (watching) {
    this.emit('end');
  } else {
    // if you want to be really specific
    process.exit(1);
  }
}

var spriteConfig = {
    selector: "icon-%f",
    cssFile: "../../sass/base/_svg-sprites.scss",
    /*
    * url("images/svg-sprites/svg/sprite.svg")
    */
    svgPath: "images/svg-sprites/%f",
    pngPath: "images/svg-sprites/%f",
    preview: false
};

// Configure Compass
var options = {
    relative    : true,
    config_file : 'css/config.rb',
    css         : 'css',
    sass        : 'css/sass',
    image       : 'css/images/',
    javascript  : 'js'
  };


// Tasks
gulp.task('webserver', function() {
  connect.server({
    port: 8001,
    livereload: true
  });
});

gulp.task('compass', function(){
  gulp.src('css/sass/*.scss')
    .pipe(plumber())
    .pipe(compass(options))
    .pipe(prefix(["last 2 versions", "ie 8"], {cascade: true}))
    .pipe(pixrem())
    .pipe(minifyCSS())
    .pipe(gulp.dest('./css/'))
    .pipe(connect.reload());
})

gulp.task('sprites', function () {
    gulp.src("css/images/svg-sprites/*.svg")
        .pipe(svgSprite(spriteConfig))
        .pipe(gulp.dest("css/images/svg-sprites"))
});

gulp.task('svg2png', function () {
    gulp.src(['css/images/svg-sprites/svg/*.svg'])
        .pipe(svg2png())
        .pipe(gulp.dest('css/images/svg-sprites/svg'));
});

gulp.task('watch', function(){
  // gulp.watch('css/images/svg-sprites/*.svg', ['sprites']);
  // gulp.watch('css/images/svg-sprites/*.svg', ['svg2png']);
  // gulp.watch(['css/sass/**/*.scss', 'css/images/sprites/**/*.png'], ['compass']);
  gulp.watch(['css/sass/**/*.scss'], ['compass']);
})

gulp.task('default', ['webserver', 'watch']);
